package com.patrickramosruas.msmachinecontrol.repositories;

import com.patrickramosruas.msmachinecontrol.models.MachineControlModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MachineControlRepository extends JpaRepository<MachineControlModel, UUID> {

}
