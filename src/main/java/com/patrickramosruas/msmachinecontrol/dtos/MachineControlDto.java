package com.patrickramosruas.msmachinecontrol.dtos;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MachineControlDto {

    @NotBlank
    private String hostname;
    @NotBlank
    private String command;
}
