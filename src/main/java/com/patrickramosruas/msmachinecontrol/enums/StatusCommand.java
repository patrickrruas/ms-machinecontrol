package com.patrickramosruas.msmachinecontrol.enums;

public enum StatusCommand {
    SENT,
    ERROR;
}
