package com.patrickramosruas.msmachinecontrol.controllers;

import com.patrickramosruas.msmachinecontrol.dtos.MachineControlDto;
import com.patrickramosruas.msmachinecontrol.models.MachineControlModel;
import com.patrickramosruas.msmachinecontrol.services.MachineControlService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MachineControlController {

    @Autowired
    MachineControlService machineControlService;

    @PostMapping("/sending-command")
    public ResponseEntity<MachineControlModel> sendindCommand(@RequestBody @Valid MachineControlDto machineControlDto){
        MachineControlModel machineControlModel = new MachineControlModel();
        BeanUtils.copyProperties(machineControlDto,machineControlModel);
        machineControlService.sendCommand(machineControlModel);

        return new ResponseEntity<>(machineControlModel, HttpStatus.ACCEPTED);
    }
}
