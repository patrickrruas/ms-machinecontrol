package com.patrickramosruas.msmachinecontrol.services;

import com.patrickramosruas.msmachinecontrol.enums.StatusCommand;
import com.patrickramosruas.msmachinecontrol.models.MachineControlModel;
import com.patrickramosruas.msmachinecontrol.repositories.MachineControlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MachineControlService {

    @Autowired
    MachineControlRepository machineControlRepository;


    public MachineControlModel sendCommand(MachineControlModel machineControlModel){
        machineControlModel.setSendCommand(LocalDateTime.now());
        System.out.println("Comando recebido!");


        machineControlModel.setStatusCommand(StatusCommand.SENT);
        return machineControlRepository.save(machineControlModel);
    }

}
