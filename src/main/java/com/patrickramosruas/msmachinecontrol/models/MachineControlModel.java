package com.patrickramosruas.msmachinecontrol.models;


import com.patrickramosruas.msmachinecontrol.enums.StatusCommand;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@Entity
@Table(name="TB_MACHINECONTROL")
public class MachineControlModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID commandID;
    private String hostname;
    @Column(columnDefinition = "TEXT")
    private String command;
    private LocalDateTime sendCommand;
    private StatusCommand statusCommand;
}
