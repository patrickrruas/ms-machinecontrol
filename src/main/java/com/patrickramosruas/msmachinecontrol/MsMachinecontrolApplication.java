package com.patrickramosruas.msmachinecontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsMachinecontrolApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMachinecontrolApplication.class, args);
	}

}
